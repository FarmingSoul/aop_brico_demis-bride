# OAP_BRICO_Demis-bride
 Demis-bride - BRICO - Outils Atelier Paysan
 
 Bride coté outil à tige ronde de 30 mm
 La tige viens s'insérer dans la douille et le fer U crée une surface plane pour accueillir la bride coté châssis .
 
 ![Demis-bride](https://framagit.org/FarmingSoul/aop_brico_demis-bride/-/raw/main/Photo/apercu.png)
 
- 12/12/22 ajout d'un repertoire CAO
- 12/12/22 ajout d'un repertoire nomenclature
- 12/12/22 ajout d'un repertoire Plans
- 12/12/22 ajout d'un repertoire Photo
